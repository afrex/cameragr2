//
//  ViewController.swift
//  cameraGr2
//
//  Created by alexfb on 28/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import MobileCoreServices
//Permite importe trabajar con videos
//import AVFoundation

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imagenView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cameraButtonPressed(_ sender: Any) {
    }
    @IBAction func rollButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePicker=UIImagePickerController()
            imagePicker.delegate=self
            //cambiar photoLibray en el caso de la camara
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing=false
            imagePicker.mediaTypes=[kUTTypeImage as String, kUTTypeMovie as String]
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        let mediaType=info[UIImagePickerControllerMediaType] as! NSString
        if mediaType.isEqual(to: kUTTypeImage as String){
            let image=info[UIImagePickerControllerOriginalImage] as! UIImage
            imagenView.image=image
            //guardar fotos en la seccion de fotos del telefono
            //UIImageWriteToSavePhotosAlbum(image, self, nil, nil)
        }
    }

}

